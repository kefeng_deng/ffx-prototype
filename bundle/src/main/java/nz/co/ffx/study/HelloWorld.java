package nz.co.ffx.study;

/**
 * @author Kefeng Deng (deng@51any.com)
 */
public class HelloWorld {

    public String say() {
        return "Hello world from HellWorld!";
    }

}
