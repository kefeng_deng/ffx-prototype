<%@page session="false"%><%
%><%@ include file="/libs/foundation/global.jsp" %><%
%><%@ page contentType="text/html; charset=utf-8" %>
<div class="page-header-content">
    <cq:include path="header" resourceType="ffx-prototype/components/header"/>
</div>
<div class="page-content">
    <cq:include script="content.jsp"/>
</div>
<div class="page-footer">
    <cq:include script="footer.jsp"/>
</div>